package com.adsi.medial1836648;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Medial1836648Application {

    public static void main(String[] args) {
        SpringApplication.run(Medial1836648Application.class, args);
    }

}
